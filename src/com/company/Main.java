package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Please enter sententce");
        Sentence sentence = new Sentence(new Scanner(System.in).nextLine());
        SentenceParser sentenceParser = new SentenceParser(sentence);

        System.out.println(sentenceParser.toString());
    }
}
