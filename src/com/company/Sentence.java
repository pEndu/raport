package com.company;

import java.util.*;
import java.util.stream.Collectors;

public class Sentence {
    private String sourceSentence;
    private String[] words;

    public Sentence(String s) {
        this.sourceSentence = s;
        this.words = sourceSentence.split(" ");
    }

    public String reverseSentence() {
        ArrayList<String> wordsList = new ArrayList<>(Arrays.asList(words));
        Collections.reverse(wordsList);

        StringBuilder sb = new StringBuilder();
        for (String word : wordsList) {
            sb.append(word + " ");
        }
        return sb.toString().trim();
    }

    public String[] getDuplicates() {
        ArrayList<String> duplicates = new ArrayList<>();
        Set wordSet = new HashSet();
        for (String word : words) {
            if (wordSet.add(word) == false) {
                duplicates.add(word);
            }
        }
        return duplicates.toArray(new String[duplicates.size()]);
    }

    public Map<String, Long> countCharacters() {
        return Arrays.stream(sourceSentence.toLowerCase().split("")).
                collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    }


    public String getSourceSentence() {
        return sourceSentence;
    }
}
