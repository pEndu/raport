package com.company;

import java.util.Arrays;
import java.util.Map;

public class SentenceParser {
    private String sourceSentence;
    private String reversedSentence;
    private String[] duplicateWords;
    private Map<String, Long> charactersCount;

    public SentenceParser(Sentence sentence) {
        this.sourceSentence = sentence.getSourceSentence();
        this.reversedSentence = sentence.reverseSentence();
        this.duplicateWords = sentence.getDuplicates();
        this.charactersCount = sentence.countCharacters();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Sentence: " + sourceSentence + "\n");
        sb.append("Reversed sentence: " + reversedSentence + "\n");
        sb.append("Duplicate words: " + Arrays.toString(duplicateWords) + "\n");
        sb.append("Characters occurency:\n");
        int sum = charactersCount.values().stream().mapToInt(Number::intValue).sum();
        for (Map.Entry<String, Long> entry : charactersCount.entrySet()) {
            Float percent = entry.getValue() * 100f / sum;
            sb.append("\"" + entry.getKey() + "\" " + String.format("%.1f", percent) + "% \n");
        }

        return sb.toString();
    }
}
